<?php
use App\Http\Controllers\UserController;
use App\Http\Controllers\TodoController;
use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Todo;
use App\Models\Tag;

Route::middleware('auth')->group(function() {
    Route::resource('/todo', TodoController::class);
    Route::put('/todos/{todo}/complete', [TodoController::class, 'complete'])->name('todo.complete');
    Route::delete('/todos/{todo}/incomplete', [TodoController::class, 'incomplete'])->name('todo.incomplete');
});


Route::get('/', function () {
    $todo = Todo::first();
    $tags = Tag::all();
    dd($todo);
    $todo->tag->attach($tags);

    dd($tags);
    // return view('welcome');

});

Route::get('/user', [UserController::class, 'index']);

Route::post('/upload', [UserController::class, 'uploadAvatar']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
